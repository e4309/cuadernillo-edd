package Lab3;

import java.util.Scanner;
import java.util.Collection;
import java.util.Collections;

public class Lab3_Lista {
    public static class Lista {
        //iniciliza el header de la lista
        private Nodo laCabeza;
        Lista() {
            laCabeza = null;
        }
        //inicializa los constructores de la lista
        private class Nodo {
            public int elObjeto;
            public Nodo next;
            public Nodo(int nuevoObjeto, Nodo next)
            {this.elObjeto=nuevoObjeto; this.next = next;}
        }
        //verifica si la lista esta vacia
        public boolean EstaVacia() {
            return laCabeza == null;
        }
        //inserta al inicio de la lista el elemento ingresado
        public void InsertaInicio(int o) {
            if (EstaVacia()) laCabeza=new Nodo(o, null);
            else  laCabeza = new Nodo(o, laCabeza);
        }
        //inserta al final de la lista el elemento ingresado
        public void InsertaFinal(int o) {
            if (EstaVacia()) laCabeza=new Nodo(o, null);
            else{
                Nodo t;
                for(t = laCabeza; t.next != null; t= t.next) ;;
                t.next = new Nodo(o,null);
            }
        }
        //obtiene el promedio del total de elementos
        public int getPromedio() {
            Nodo n = laCabeza;
            int contador = 0;
            int tamano = 0;
            while (n != null) {
                contador += n.elObjeto;
                tamano++;
                n = n.next;
            }
            return contador/tamano;
        }
        //obtiene los valores para el calculo del promedio
        public Lista selectValores(int promedio) {
            Lista mayores = new Lista();
            Nodo n = laCabeza;
            while (n != null) {
                if (n.elObjeto > promedio) {
                    mayores.InsertaFinal(n.elObjeto);
                }
                n = n.next;
            }
            return mayores;
        }

    /*public Lista createLista() {

    }*/
    }

    public static void main(String[] args) {
        Lista l = new Lista();
        Scanner tec = new Scanner(System.in);
        System.out.println("Ingrese un dato: ");
        int a = tec.nextInt();
        l.InsertaFinal(a);
        System.out.println("Ingrese un dato: ");
        int b = tec.nextInt();
        l.InsertaFinal(b);
        System.out.println("Ingrese un dato: ");
        int c = tec.nextInt();
        l.InsertaFinal(c);
        System.out.println("Ingrese un dato: ");
        int d = tec.nextInt();
        int promedio = l.getPromedio();
        l.InsertaInicio(d);
        l.InsertaFinal(d);
        l.selectValores(promedio);
        System.out.println("Lista Original");
        //l.Print;
        Lista n = l.selectValores(promedio);
        System.out.println("Lista Ordenada");
        //n.Print();
        //Collections.sort(l);
        System.out.println(" ");
        System.out.println("Promedio: " + promedio);
    }

}
