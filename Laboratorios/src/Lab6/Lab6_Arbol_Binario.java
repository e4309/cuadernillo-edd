package Lab6;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class Lab6_Arbol_Binario {
    public static class ArbolBinario {
        //inicializa atributos del arbol binario
        Scanner tec = new Scanner(System.in);
        int dato;
        ArbolBinario iz;
        ArbolBinario der;
        private Object nodos;

        //constructor base del arbol
        public ArbolBinario(int dato) {
            this.dato = dato;
            iz = null;
            der = null;
        }

        //constructor con hijos del arbol
        public ArbolBinario(int dato, ArbolBinario iz, ArbolBinario der) {
            this.dato = dato;
            this.iz = iz;
            this.der = der;
        }

        //ordena el arbol en preorden
        public void preOrden() {
            System.out.print(this.dato + " ");
            if (iz != null) iz.preOrden();
            if (der != null) der.preOrden();
        }

        //ordena el arbol en inorden
        public void inOrden() {
            if (iz != null) iz.inOrden();
            System.out.print(this.dato + " ");
            if (der != null) der.inOrden();
        }

        //ordena el arbol en postorden
        public void posOrden() {
            if (iz != null) iz.posOrden();
            if (der != null) der.posOrden();
            System.out.print(this.dato + " ");
        }

        //cantidad de nodos-1 del camino más largo de la raiz a sus hojas
        public int altura() {
            return altura(this);
        }

        private int altura(ArbolBinario raiz) {
            if (raiz == null) {
                return -1;
            }
            return 1 + Math.max(altura(raiz.iz), altura(raiz.der));
        }

        //size: cantidad de nodos del árbol
        public int size() {
            return size(this);
        }

        //funcion ayuda
        private int size(ArbolBinario raiz) {
            if (raiz == null) {
                return 0;
            }
            return 1 + size(raiz.iz) + size(raiz.der);
        }

        //devuelve el arbol y sus eslavones
        public void tree() {
            tree(this, "");
        }

        //funcion ayuda
        private void tree(ArbolBinario raiz, String tab) {
            if (raiz != null) {
                System.out.println(tab + "->" + raiz.dato);
                tree(raiz.iz, tab + "  |");
                tree(raiz.der, tab + "  ");
            }
        }

        //regresa los nodos por altura ingresada por teclado
        public void nodosAlturas(int h) {
            System.out.println("Obtener derecha");
            if (der != null) der.nodosAlturas(h);
            System.out.println("Obtener izquierda");
            if (iz != null) iz.nodosAlturas(h);

            if (this.altura() == h) {
                System.out.println(this.dato);
            }
        }

        //funcion ayuda
        public LinkedList<ArbolBinario> nodosAltura(int h) {
            LinkedList<ArbolBinario> nodo = new LinkedList<>();
            if (iz != null) iz.preOrden();
            if (der != null) der.preOrden();
            if (iz != null) iz.nodosAltura(h);
            if (der != null) der.nodosAltura(h);
            if (this.altura() == h) {
                nodo.add(this);
            }
            return nodo;
        }
    }

    public static void main(String[] args) {
        // write your code here
        Scanner tec = new Scanner(System.in);
        Stack<ArbolBinario> nodos=new Stack<>();
        for (int i = 0; i < 10; i++) {
            nodos.add(new ArbolBinario(i));
        }
        //voy emparejando de a dos nodos para construir el árbol
        while(nodos.size()>1){
            ArbolBinario iz=nodos.pop();
            ArbolBinario der=nodos.pop();
            nodos.push(new ArbolBinario(iz.dato+der.dato,iz,der));
        }
        ArbolBinario raiz=nodos.pop();
        System.out.println("Altura del arbol: "+raiz.altura());
        System.out.println("Tamaño del arbol (total de nodos): "+ raiz.size());
        raiz.tree();

        ArbolBinario[] nodos2=new ArbolBinario[32];
        for (int i = 0; i < nodos2.length; i++) {
            nodos2[i]=new ArbolBinario(i);
        }
        ArbolBinario raiz2=generaBinario(nodos2, 0, 31);
        System.out.println("Altura del arbol: "+raiz2.altura());
        System.out.println("Tamaño del arbol (total de nodos): "+ raiz2.size());
        raiz2.tree();

        System.out.println("Los nodos de la altura son: ");
        int h = tec.nextInt();
        raiz.nodosAlturas(h);
    }

    public static ArbolBinario generaBinario(ArbolBinario[] nodos, int i, int j){
        if(i>j){
            //caso base
            return null;
        }
        int mitad=(i+j)/2;
        nodos[mitad].iz=generaBinario(nodos, i,mitad-1);
        nodos[mitad].der=generaBinario(nodos,mitad+1,j);
        return nodos[mitad];
    }
}
