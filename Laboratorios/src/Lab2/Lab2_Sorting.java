package Lab2;

import java.util.Arrays;
import java.util.Random;

public class Lab2_Sorting {
    public static int[] fillArray(int n) {
        int[] array = new int[n];
        Random rnd = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rnd.nextInt(n * 10);
        }
        return array;
    }

    public static void bubbleSort(int array[]) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    // swap arr[j+1] and arr[j]
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void merge(int array[], int l, int m, int r) {
        // Find sizes of two subarrays to be merged
        int arr1 = m - l + 1;
        int arr2 = r - m;

        /* Create temp arrays */
        int list1[] = new int[arr1];
        int list2[] = new int[arr2];

        /* Copy data to temp arrays */
        for (int i = 0; i < arr1; ++i)
            list1[i] = array[l + i];
        for (int j = 0; j < arr2; ++j)
            list2[j] = array[m + 1 + j];

        /* Merge the temp arrays */

        // Initial indexes of first and second subarrays
        int i = 0, j = 0;

        // Initial index of merged subarray array
        int k = l;
        while (i < arr1 && j < arr2) {
            if (list1[i] <= list2[j]) {
                array[k] = list1[i];
                i++;
            } else {
                array[k] = list2[j];
                j++;
            }
            k++;
        }

        /* Copy remaining elements of list1[] if any */
        while (i < arr1) {
            array[k] = list1[i];
            i++;
            k++;
        }

        /* Copy remaining elements of list2[] if any */
        while (j < arr2) {
            array[k] = list2[j];
            j++;
            k++;
        }
    }

    // Main function that sorts arr[l..r] using
    // merge()
    public static void sort(int array[], int l, int r) {
        if (l < r) {
            // Find the middle point
            int m = l + (r - l) / 2;

            // Sort first and second halves
            sort(array, l, m);
            sort(array, m + 1, r);

            // Merge the sorted halves
            merge(array, l, m, r);
        }
    }

    public static void mergeSort(int array[]) {
        sort(array, 0, array.length - 1);
    }

    public static void print(int array[]) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }


    //funcion rellena arrays y lista el tamaño de cada una, posterior lista el tiempo en un arreglo
    public static void main(String[] args) {
        int testnum[] = {1000,5000,10000,50000,100000,1000000,5000000,10000000,50000000};
        long data[][] = new long[9][3];
        for(int i = 0; i< testnum.length; i++){
            System.out.println("Tamaño: " + testnum[i]);
            int arr1[] = fillArray(testnum[i]);
            int arr2[] = fillArray(testnum[i]);
            int arr1_1[] = arr1.clone();
            int arr2_1[] = arr2.clone();
            int arr1_2[] = arr1.clone();
            int arr2_2[] = arr2.clone();
            long startTime = 0;
            long finishTime = 0;
            if(testnum[i] < 1000000){
                startTime = System.nanoTime();
                bubbleSort(arr1);
                finishTime = System.nanoTime() - startTime;
                data[i][0] = finishTime;

                startTime = System.nanoTime();
                bubbleSort(arr2);
                finishTime = System.nanoTime() - startTime;
                data[i][0] = ((data[i][0] + finishTime) / 2)/1000;

            }else{
                data[i][0] = -1;
            }
            startTime = System.nanoTime();
            mergeSort(arr1_1);
            finishTime = System.nanoTime() - startTime;
            data[i][1] = finishTime;

            startTime = System.nanoTime();
            mergeSort(arr2_1);
            finishTime = System.nanoTime() - startTime;
            data[i][1] = ((data[i][0] + finishTime) / 2)/1000;
            startTime = System.nanoTime();
            Arrays.sort(arr1_2);
            finishTime = System.nanoTime() - startTime;
            data[i][2] = finishTime;

            startTime = System.nanoTime();
            Arrays.sort(arr2_2);
            finishTime = System.nanoTime() - startTime;
            data[i][2] = ((data[i][0] + finishTime) / 2)/1000;


        }

        System.out.printf("%-12s %-12s %-11s %-12s%n","Tamaño","Bubble Sort","Merge Sort","Arrays sort");
        for(int i=0; i< testnum.length; i++){
            System.out.printf("%-12s %-12s %-11s %-12s%n",testnum[i],data[i][0],data[i][1],data[i][2]);
        }

    }
}
