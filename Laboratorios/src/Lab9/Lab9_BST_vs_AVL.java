package Lab9;

public class Lab9_BST_vs_AVL {
    public static class AvlTree{
        //inicializa los atributos del arbol
        private class AvlNode{

            public int value;
            public int height;
            public AvlNode right;
            public AvlNode left;
            //inicializa el constructor del arbol
            public AvlNode(int value) {
                this.value = value;
                height = 1;
                right = null;
                left = null;
            }

        }
        //inicializa la raiz del arbol
        private AvlNode root;
        //inicializa la raiz en nulo
        public AvlTree() {
            root = null;
        }
        //funcion buscar en el arbol
        public boolean search(int value) {
            return search(value, root);
        }
        //funcion ayuda
        private boolean search(int value, AvlNode node) {
            if(node == null) return false;
            if(node.value == value) return true;
            if(value < node.value) return search(value, node.left);
            return search(value, node.right);
        }
        //inserta elementos en el arbol pasando un valor por teclado
        public void insert(int value) {
            if(root == null) {
                root = new AvlNode(value);
            }else{
                root = insert(value, root);
            }
            root = balance(root);

        }
        //funcion ayuda
        private AvlNode insert(int value, AvlNode node) {

            if(value < node.value){
                if(node.left == null) {
                    node.left = new AvlNode(value);
                }else {
                    node.left = insert(value, node.left);
                }
            }else if(node.value < value) {
                if(node.right == null) {
                    node.right = new AvlNode(value);
                }else {
                    node.right = insert(value, node.right);
                }
            }

            node = balance(node);
            return node;
        }
        //balancea el arbol segun el formato AVL
        private AvlNode balance(AvlNode root) {
            int balanceIndex = getHeight(root.left) - getHeight(root.right);
            AvlNode result = root;
            if(balanceIndex > 1) {
                int heightLeftLeft = getHeight(root.left.left);
                int heightLeftRight = getHeight(root.left.right);
                if(heightLeftLeft > heightLeftRight) {
                    result = leftLeft(root);
                }else {
                    result = leftRight(root);
                }
            }else if(balanceIndex < -1) {
                int heightRightLeft = getHeight(root.right.left);
                int heightRightRight = getHeight(root.right.right);
                if(heightRightLeft > heightRightRight) {
                    result = rightLeft(root);
                }else {
                    result = rightRight(root);
                }
            }else {
                result.height = getHeight(result); // Actualizaci�n de la altura
            }
            return result;
        }

        /* Left Left case */
        private AvlNode leftLeft(AvlNode root) {
            return rightRotate(root);
        }

        /* Left Right Case */
        private AvlNode leftRight(AvlNode root) {
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        /* Right Left Case */
        private AvlNode rightLeft(AvlNode root) {
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }

        /* Right Right Case */
        private AvlNode rightRight(AvlNode root) {
            AvlNode result = leftRotate(root);
            return result;
        }
        //rota el arbol a la derecha
        private AvlNode rightRotate(AvlNode root) {
            AvlNode left = root.left;
            root.left = left.right;
            left.right = root;

            // Actualizacion de las alturas
            root.height = getHeight(root);
            left.height = getHeight(left);
            return left;
        }
        //rota el arbol a la izquierda
        private AvlNode leftRotate(AvlNode root) {
            AvlNode right = root.right;
            root.right = right.left;
            right.left = root;

            // Actualizacion de las alturas
            root.height = getHeight(root);
            right.height = getHeight(right);
            return right;
        }
        //obtiene la altura del arbol
        private int getHeight(AvlNode node) {
            if(node == null) return 0;
            return Math.max(node.left != null ? node.left.height : 0, node.right != null ? node.right.height : 0)+1;
        }
        //imprime el arbol
        public void print() {
            print(root, "");
        }
        //funcion ayuda
        private void print(AvlNode node, String space) {
            if(node == null) {
                System.out.println(space + "NULL");
            }else {
                System.out.println(space + "[" + node.value + ", " + node.height + "]");
                print(node.left, space+"--");
                print(node.right, space+"--");
            }

        }

    }

    public static class BSTree{

        private class BSTNode{

            public int value;
            public BSTNode right;
            public BSTNode left;

            public BSTNode(int value) {
                this.value = value;
                right = null;
                left = null;
            }

        }

        private BSTNode root;

        public BSTree() {
            root = null;
        }

        public boolean search(int value) {
            return search(value, root);
        }

        private boolean search(int value, BSTNode node) {
            if(node == null) return false;
            if(node.value == value) return true;
            if(value < node.value) return search(value, node.left);
            return search(value, node.right);
        }

        public void insert(int value) {
            if(root == null) {
                root = new BSTNode(value);
            }else{
                root = insert(value, root);
            }

        }

        private BSTNode insert(int value, BSTNode node) {

            if(value < node.value){
                if(node.left == null) {
                    node.left = new BSTNode(value);
                }else {
                    node.left = insert(value, node.left);
                }
            }else if(node.value < value) {
                if(node.right == null) {
                    node.right = new BSTNode(value);
                }else {
                    node.right = insert(value, node.right);
                }
            }

            return node;
        }

        public void print() {
            print(root, "");
        }

        private void print(BSTNode node, String space) {
            if(node == null) {
                System.out.println(space + "NULL");
            }else {
                System.out.println(space + "[" + node.value + "]");
                print(node.left, space+"--");
                print(node.right, space+"--");
            }

        }

    }

    public static void main(String[] args) {
        AvlTree AVLTree = new AvlTree();
        BSTree BSTree = new BSTree();

        double tiempoInicialAVL;
        double tiempoFinalAVL;

        tiempoInicialAVL = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            AVLTree.insert(i);
        }
        tiempoFinalAVL = System.currentTimeMillis();
        double tiempoTotalAVL = tiempoFinalAVL - tiempoInicialAVL;
        System.out.println("El tiempo final es: " + tiempoTotalAVL + " segs");

        double tiempoInicialBST;
        double tiempoFinalBST;

        tiempoInicialBST = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            BSTree.insert(i);
        }
        tiempoFinalBST = System.currentTimeMillis();
        double tiempoTotalBST = tiempoFinalBST - tiempoInicialBST;
        System.out.println("El tiempo final es: " + tiempoTotalBST + " segs");
    }

}
