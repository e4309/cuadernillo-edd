package Lab4;

import java.util.LinkedList;
import java.util.Queue;

public class Lab4_LeetCode {
    public class ListNode{
        int top;
        int tam;
        char [] data;

        public ListNode(int tam){
            this.tam=tam;
            data=new char [tam];
            top = -1;
        }

        public void Push(char x){
            data[++top]=x;
        }

        public char Pop(){
            return data[top--];
        }

        public char Top() {
            return data[top];
        }
    }

    class Solution {
        //Metodo permite para calcular el tamaño de los parentesis mientras sea valido y elimina al final el ultimo dato
        public int maxDepth(String s) {
            ListNode list = new ListNode(s.length());
            for (int i = s.length() - 1; i >= 0; i--) {
                list.Push(s.charAt(i));
            }
            int result = 0, cant = 0;
            for (int i = 0; i < s.length(); i++) {
                if (list.Top() == '(') {
                    cant++;

                } else if (list.Top() == ')') {
                    cant--;
                }
                result = Math.max(result, cant);
                list.Pop();
            }
            return result;
        }

    }

    class Main {
        //Cola para almacenar las llamadas
        Queue<Integer> Counter;
        public Main() {
            Counter = new LinkedList<>();

        }
        //Añade una llamada a la cola y retorna las ocurridas en los ultimos 3000 milisegundos
        public int ping(int t) {
            Counter.add(t);
            while (Counter.peek() < t - 3000) {
                Counter.poll(); }
            return Counter.size();
        }

    }


}
