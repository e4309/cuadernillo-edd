package Lab14;
//Integrantes Sergio Villanueva, Fernando Vergara, Johnson Valenzuela, Celso San Martin
//Sergio Ruiz, Vicente Espoz, Tomas Yevenes, Sebastian Orellana

import java.util.Arrays;

//REFERENCIA: https://leetcode.com/problems/maximum-total-importance-of-roads/discuss/2113923/Simple-Java-Solution

public class Lab14_Grafos {
    public class Solution {
    /* Se le da un número entero n que denota el número de ciudades en un país.
    Las ciudades están numeradas del 0 al n - 1.
    Debe asignar a cada ciudad un valor entero del 1 al n, donde cada valor solo se puede usar una vez.
    La importancia de una carretera se define entonces como la suma de los valores de las
    dos ciudades que conecta.*/

        //Devuelve la máxima importancia total de todas las carreteras posibles después de asignar los valores de forma óptima.
        //Por facilitacion de LeetCode la solucion sera de tipo long.
        public long maximumImportance(int n, int[][] roads) {

            //Crea la un arreglo unidimensional de tipo long de tamaño pasado por parametro.
            //El arreglo de long se encargara de almacenar las ciudades.
            long[] ciudades = new long[n];

            //Recorremos el arreglo de caminos con for each.
            for (int[] caminos : roads) {
                //Mientras se recorre se añaden los caminos al arreglo ciudades con un contador de aumento.
                //Aumenta de tamaño el arreglo caminos por cada nuevo camino añadido.
                ciudades[caminos[0]]++;
                ciudades[caminos[1]]++;
            }

            //Se realiza sorting (ordenar) al arreglo previamente rellenado con las ciudades.
            //Ordenara el arreglo de los caminos por su numero de forma ascendente.
            Arrays.sort(ciudades);

            //Se inicializa el dato de retorno de tipo long.
            //Este almacenara el valor maximos de caminos que tiene una ciudad.
            long suma = 0;

            //Se recorre con for la lista de ciudades y sus caminos.
            for (int i = 0; i < n; i++)
                //Se realiza con un operador de asignacion de aumento a la variable suma con el arreglo caminos.
                //Esta almacenara el valor de la ciudad que posee mas caminos aumentando el tamaño de este.
                //Ademas guardara el numero de cada ciudad y su numero de caminos.
                suma += (i + 1) * ciudades[i];

            //Retorna el valor de la ciudad que posee mas caminos.
            return suma;
        }
    }
}
