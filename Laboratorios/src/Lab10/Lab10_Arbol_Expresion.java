package Lab10;

import java.util.Scanner;
import java.util.Stack;

public class Lab10_Arbol_Expresion {
    public static class ArbolExpresion {

        public ArbolExpresion left;
        public ArbolExpresion right;
        public char symbol;

        public ArbolExpresion(char symbol) {
            this.symbol = symbol;
        }

        public void print(int number) {
            String line = "";
            for (int i = 0; i < number; i++) {
                line += "--";
            }
            line += "> " + this.symbol;
            System.out.println(line);
            if (left != null) left.print(number + 1);
            if (right != null) right.print(number + 1);
        }
    }

    public class Parsing {
        //Método pasa el arbol de un orden infijo a uno postfijo con la ayuda de una pila
        public static String fromInfijoToPostfijo(String infijo) {
            //Inicializa un string de resultado
            String suma = "";
            //Inicializa una pila de tipo Character
            Stack<Character> Pila = new Stack<>();
            //Crea un arreglo de tipo char para almacenar cada valor.
            char[] vals = infijo.toCharArray();
            //Para cada vals revisará el valor de cada uno según su prioridad
            for (char x : vals) {
                //Si el digito es un numero, se agregara al resultado
                if (isANumber(x)) {
                    suma += x;
                    //Si el digito es un paréntesis, lo guardara en la pila
                } else if (priority(x) == 0) {
                    Pila.push(x);
                    //Si es un paréntesis cerrado, va a liberar los valores de la y agregarlos.
                } else if (priority(x) == -1) {
                    while (Pila.peek() != '(') {
                        //hasta que sea un paréntesis cerrado.
                        suma += Pila.pop();
                    }
                    Pila.pop();
                    //Si el dígito es un operador, comprobara si la pila esta vacía
                } else if (priority(x) == 1 || priority(x) == 2) {
                    if (Pila.isEmpty()) {
                        //Lo Sacara de la pila
                        Pila.push(x);
                    } else {
                        if (priority(x) > priority(Pila.peek())) {
                            Pila.push(x);
                        } else {
                            suma += Pila.pop();
                            Pila.push(x);
                        }
                    }
                }
            }
            //Finalmente agrega cualquier valor aun almacenado en la pila
            while (!Pila.isEmpty()) {
                suma += Pila.pop();
            }
            //y retorna dicho resultado.
            return suma;
        }

        //Revisa si el digito ingreado es numero
        private static boolean isANumber(char symbol) {
            return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' ||
                    symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' ||
                    symbol == '8' || symbol == '9';
        }

        //Revisa si el digito es un signo de prioridad
        private static int priority(char symbol) {
            if (symbol == '(') return 0;
            if (symbol == '+' || symbol == '-') return 1;
            if (symbol == '*' || symbol == '/') return 2;
            return -1;
        }

        //Método sirve para construir el árbol expresión dado de forma postfija
        public static ArbolExpresion getArbol(String postfijo) {
            //Establece una pila de árbol expresión para almacenarlo
            Stack<ArbolExpresion> Pila = new Stack<>();
            //Crea un arreglo para almacenar los Char como tokens
            char[] tokens = postfijo.toCharArray();
            //Revisa cada tokens si este es un número o un signo de prioridad
            for (char s : tokens) {
                ArbolExpresion A = new ArbolExpresion(s);
                //Verifica si s es un numero
                if (isANumber(s)) {
                    Pila.push(A);
                    //De lo contario lo saca de la pila
                } else {
                    A.left = Pila.pop();
                    A.right = Pila.pop();
                    Pila.push(A);
                }
            }
            //retorna el tope de la pila
            return Pila.peek();
        }

        //Método entrega el resultado del árbol expresión
        public static int Calculadora(ArbolExpresion x) {
            if (x == null) {
                return 0;
            }
            //Establece dos int valor, iguales al método de calculadora
            int valor = Calculadora(x.right);
            int result = 0;
            //Si el symbol del nodo es un número, result será el valor numérico del carácter
            if (isANumber(x.symbol)) {
                result = Character.getNumericValue(x.symbol);
                //Si la prioridad es un operador, result será el dato de valor sumado.
            } else if (priority(x.symbol) == 1 || priority(x.symbol) == 2) {
                //Un switch para cada caso de simbolos
                switch (x.symbol) {
                    case '+':
                        result = valor + Calculadora(x.left);
                        break;
                    case '-':
                        result = valor - Calculadora(x.left);
                        break;
                    case '*':
                        result = valor * Calculadora(x.left);
                        break;
                    case '/':
                        result = valor / Calculadora(x.left);
                        break;
                }
            }
            //Retorna el resultado.
            return result;
        }
    }


    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner tec = new Scanner(System.in);
        System.out.print("Ingrese su expresi�n: ");
        String expresion = tec.next();
        tec.close();
        String salida = Parsing.fromInfijoToPostfijo(expresion);
        System.out.print("Su expresi�n postfija es: " + salida);
        ArbolExpresion a = Parsing.getArbol(salida);
        System.out.println("\n\nSU ARBOL DE EXPRESION: \n");
        a.print(0);
        int x = Parsing.Calculadora(a);
        System.out.println("\n\nEL RESULTADO ES: \n");
        System.out.println(x);

    }

}
