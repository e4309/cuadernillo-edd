package Lab7;

import java.util.LinkedList;

public class Lab7_Complemento {
    public static class Abb {
        class NodoAbb {
            int elemento;
            NodoAbb lchild;
            NodoAbb rchild;

            NodoAbb(int elemento, NodoAbb lchild, NodoAbb rchild) {
                this.elemento = elemento;
                this.lchild = lchild;
                this.rchild = rchild;
            }

            void Print() {
                System.out.println(elemento);
            }
        }

        private NodoAbb laRaiz;

        public Abb() {
            laRaiz = null;
        }

        /* Verifica si existen dos enteros a, b en el ABB tal que a+b =0.
         */
        public boolean Complemento() {
            LinkedList<Integer> datos = ListaIn(this.laRaiz);
            for(int k = 0; k< datos.size(); k++){

                if(searchBSS(laRaiz, datos.get(k)*-1)){
                    return true;
                }
            }
            return false;

        }
        //funcion ayuda para identificar si el complemento
        private LinkedList<Integer> ListaIn(NodoAbb x){
            if(x==null) {
                return null;
            }
            LinkedList<Integer> comple = new LinkedList<>();
            comple.add(x.elemento);
            if(x.lchild!=null) {
                LinkedList<Integer> left = ListaIn(x.lchild);
                for (int k = 0; k < left.size(); k++) {
                    comple.add(left.get(k));
                }
            }
            if(x.rchild!=null){
                LinkedList<Integer> right = ListaIn(x.rchild);
                for(int k = 0; k< right.size(); k++){
                    comple.add(right.get(k));
                }
            }
            return comple;

        }

        //funcion busca dentro del arbol segun una clave pasado por teclado
        private boolean searchBSS(NodoAbb x, int k) {
            if(x == null){
                return false;
            }else if(k == x.elemento){
                return true;
            }
            if(k < x.elemento){
                return searchBSS(x.lchild, k);
            }else{
                return searchBSS(x.rchild, k);
            }

        }

        //--- Supone que no existe un nodo con valor = elemento----//
        public void Insertar(int elemento) {
            laRaiz = InsertaenAbb(laRaiz, elemento);
        }

        private NodoAbb InsertaenAbb(NodoAbb nodo, int elemento) {
            if (nodo == null)
                return new NodoAbb(elemento, null, null);
            else if (elemento < nodo.elemento)
                nodo.lchild = InsertaenAbb(nodo.lchild, elemento);
            else
                nodo.rchild = InsertaenAbb(nodo.rchild, elemento);
            return nodo;
        }

        // -- Supone que el elemento esta en el arbol----//
        public void Eliminar(int elemento) {
            laRaiz = EliminaenAbb(laRaiz, elemento);
        }
        //elimina un elemento pasando una clave y la altura del root
        private NodoAbb EliminaenAbb(NodoAbb root, int elemento) {
            if (root.elemento == elemento) {
                if (root.lchild == null && root.rchild == null)
                    return null;
                else if (root.lchild == null)
                    return root.rchild;
                else if (root.rchild == null)
                    return root.lchild;
                else {
                    root.elemento = MayorElemento(root.lchild);
                    root.lchild = EliminaenAbb(root.lchild, root.elemento);
                }
            } else if (root.elemento > elemento)
                root.lchild = EliminaenAbb(root.lchild, elemento);
            else
                root.rchild = EliminaenAbb(root.rchild, elemento);
            return root;
        }
        //devuelve el mayor elemento de un nodo
        private int MayorElemento(NodoAbb nodo) {
            if (nodo.rchild == null)
                return nodo.elemento;
            else
                return MayorElemento(nodo.rchild);
        }
        //imprime el arbol
        public void Imprimir() {
            ImprimeAbb(laRaiz, " ");
        }

        private void ImprimeAbb(NodoAbb n, String tab) {
            if (n != null) {
                System.out.println(tab + n.elemento);
                ImprimeAbb(n.lchild, tab + "  ");
                ImprimeAbb(n.rchild, tab + "  ");
            }
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Abb a = new Abb();
        a.Insertar(10);
        a.Insertar(8);
        a.Insertar(12);
        a.Insertar(4);
        a.Insertar(-8);

        a.Imprimir();

        boolean root = a.Complemento();
        System.out.println(" EL complemento es : " + a.Complemento());	// True
        a.Eliminar(-8);
        root = a.Complemento();
        System.out.println(" EL complemento es : " + a.Complemento());	// False

    }

}
