package Lab13;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Lab13_Grafos {

    //REFERENCIA: https://leetcode.com/problems/find-center-of-star-graph/discuss/2390126/Java-Solution-Easy

    //Funcion para encontrar el centro de la estrella
    public int findCenter(int[][] edges) {
        //Inicializa las posiciones
        int edgeOne = edges[0][0];
        int edgeTwo = edges[0][1];

        //Verifica las posiciones de la estrella
        if(edgeOne == edges[1][0] || edgeOne == edges[1][1])
            //Retonara la posicion 1 si no cumple la primera o la segunda condicion
            return edgeOne;
        else
            //retornara la posicion 2 si cumple ambas condiciones
            return edgeTwo;
    }

    //REFERENCIA: https://leetcode.com/problems/find-if-path-exists-in-graph/discuss/2479874/My-short-java-solution

    //Funcion que valida si el camino es valido
    public boolean validPath(int n, int[][] edge, int sv, int ev) {
        if(edge.length <= 1) return true;
        List<List<Integer>> adj = new ArrayList<>();
        for(int i = 0; i < n; i++){
            adj.add(new ArrayList<>());
        }

        for(int[] arr : edge){
            adj.get(arr[0]).add(arr[1]);
            adj.get(arr[1]).add(arr[0]);
        }
        Queue<Integer> queue = new LinkedList<>();
        boolean[] visited = new boolean[n];
        queue.add(sv);
        while(!queue.isEmpty()){
            int vertex = queue.poll();
            visited[vertex] = true;
            for(int i = 0; i < adj.get(vertex).size(); i++){
                if(adj.get(vertex).get(i) == ev) return true;
                if(!visited[adj.get(vertex).get(i)]){
                    queue.add(adj.get(vertex).get(i));
                }
            }
        }
        return false;
    }

    //REFERENCIA: https://leetcode.com/problems/find-the-town-judge/discuss/2478751/Java-or-One-Array-or-No-HashMap-or-Fastest

    //Funcion para encontrar al juez del pueblo
    public int findJudge(int n, int[][] trust) {
        //Inicializa a la gente de confianza del juez
        int[] trusting = new int[n];
        int[] trustedBy = new int[n];
        //recorre el arreglo de confianza
        for(int i=0;i<trust.length;i++){
            //rellena con las personas
            trusting[trust[i][0]-1]++;
            trustedBy[trust[i][1]-1]++;
        }
        for(int i=0;i<n;i++){
            //verifica si la gente es de confianza
            if(trustedBy[i] == n-1){
                if(trusting[i] == 0){
                    //Si cumple las caracteristicas anteriores es el juez
                    return i+1;
                }
            }
        }
        // De lo contario retornara que es no es el juez
        return -1;
    }
}
