package Lab15;

//REFERENCIA: https://github.com/csanjuanc/estructuras-de-datos
public class Lab15_Arbol_B {
    public static class ArbolB {
        NodoArbolB root;
        int t;

        // Constructor
        public ArbolB(int t) {
            this.t = t;
            root = new NodoArbolB(t);
        }

        //Funcion ayuda paraa mostrar la clave maxima
        public int buscarClaveMayor() {
            int claveMaxima = getClaveMayor(this.root);

            return claveMaxima;
        }

        private int getClaveMayor(NodoArbolB current) {
            if (current == null) {
                return 0;// Si es cero no existe
            }

            // Mientras no sea una hoja
            while (!current.leaf) {
                // Se accede al hijo mas a la derecha
                current = current.child[current.n];
            }

            return claveMayorPorNodo(current);
        }

        private int claveMayorPorNodo(NodoArbolB current) {
            // Devuelve el valor mayor, el que esta mas a la derecha
            return current.key[current.n - 1];
        }

        public void mostrarClavesNodoMinimo() {
            //Imprime la claves
            NodoArbolB temp = buscarNodoMinimo(root);

            if (temp == null) {
                System.out.println("Sin minimo");
            } else {
                temp.imprimir();
            }
        }

        public NodoArbolB buscarNodoMinimo(NodoArbolB nodoActual) {
            //Busca el nodo minimo en el arbol
            if (root == null) {
                return null;
            }

            NodoArbolB aux = root;

            // Mientras no sea una hoja
            while (!aux.leaf) {
                // Se accede al primer hijo
                aux = aux.child[0];
            }

            // Devuelve el nodo menor, el que esta mas a la izquierda
            return aux;
        }

        // Busca el valor ingresado y muestra el contenido del nodo que contiene el valor
        public void buscarNodoPorClave(int num) {
            NodoArbolB temp = search(root, num);

            if (temp == null) {
                System.out.println("No se ha encontrado un nodo con el valor ingresado");
            } else {
                print(temp);
            }
        }

        // Search
        private NodoArbolB search(NodoArbolB actual, int key) {
            int i = 0;// se empieza a buscar siempre en la primera posicion

            // Incrementa el indice mientras el valor de la clave del nodo sea menor
            while (i < actual.n && key > actual.key[i]) {
                i++;
            }

            // Si la clave es igual, entonces retornamos el nodo
            if (i < actual.n && key == actual.key[i]) {
                return actual;
            }

            // Si llegamos hasta aqui, entonces hay que buscar los hijos
            // Se revisa primero si tiene hijos
            if (actual.leaf) {
                return null;
            } else {
                // Si tiene hijos, hace una llamada recursiva
                return search(actual.child[i], key);
            }
        }

        public void insertar(int key) {
            NodoArbolB r = root;

            // Si el nodo esta lleno lo debe separar antes de insertar
            if (r.n == ((2 * t) - 1)) {
                NodoArbolB s = new NodoArbolB(t);
                root = s;
                s.leaf = false;
                s.n = 0;
                s.child[0] = r;
                split(s, 0, r);
                nonFullInsert(s, key);
            } else {
                nonFullInsert(r, key);
            }
        }

        // Caso cuando la raiz se divide
        // x = | | | | | |
        // /
        // |10|20|30|40|50|
        // i = 0
        // y = |10|20|30|40|50|
        private void split(NodoArbolB x, int i, NodoArbolB y) {
            // Nodo temporal que sera el hijo i + 1 de x
            NodoArbolB z = new NodoArbolB(t);
            z.leaf = y.leaf;
            z.n = (t - 1);

            // Copia las ultimas (t - 1) claves del nodo y al inicio del nodo z // z =
            // |40|50| | | |
            for (int j = 0; j < (t - 1); j++) {
                z.key[j] = y.key[(j + t)];
            }

            // Si no es hoja hay que reasignar los nodos hijos
            if (!y.leaf) {
                for (int k = 0; k < t; k++) {
                    z.child[k] = y.child[(k + t)];
                }
            }

            // nuevo tamanio de y // x = | | | | | |
            y.n = (t - 1); // / \
            // |10|20| | | |
            // Mueve los hijos de x para darle espacio a z
            for (int j = x.n; j > i; j--) {
                x.child[(j + 1)] = x.child[j];
            }
            // Reasigna el hijo (i+1) de x // x = | | | | | |
            x.child[(i + 1)] = z; // / \
            // |10|20| | | | |40|50| | | |
            // Mueve las claves de x
            for (int j = x.n; j > i; j--) {
                x.key[(j + 1)] = x.key[j];
            }

            // Agrega la clave situada en la mediana // x = |30| | | | |
            x.key[i] = y.key[(t - 1)]; // / \
            x.n++; // |10|20| | | | |40|50| | | |
        }

        private void nonFullInsert(NodoArbolB x, int key) {
            // Si es una hoja
            if (x.leaf) {
                int i = x.n; // cantidad de valores del nodo
                // busca la posicion i donde asignar el valor
                while (i >= 1 && key < x.key[i - 1]) {
                    x.key[i] = x.key[i - 1];// Desplaza los valores mayores a key
                    i--;
                }

                x.key[i] = key;// asigna el valor al nodo
                x.n++; // aumenta la cantidad de elementos del nodo
            } else {
                int j = 0;
                // Busca la posicion del hijo
                while (j < x.n && key > x.key[j]) {
                    j++;
                }

                // Si el nodo hijo esta lleno lo separa
                if (x.child[j].n == (2 * t - 1)) {
                    split(x, j, x.child[j]);

                    if (key > x.key[j]) {
                        j++;
                    }
                }

                nonFullInsert(x.child[j], key);
            }
        }

        public void showBTree() {
            print(root);
        }

        // Print en preorder
        private void print(NodoArbolB n) {
            n.imprimir();

            // Si no es hoja
            if (!n.leaf) {
                // recorre los nodos para saber si tiene hijos
                for (int j = 0; j <= n.n; j++) {
                    if (n.child[j] != null) {
                        System.out.println();
                        print(n.child[j]);
                    }
                }
            }
        }

        //Método permite saber la cantidad de datos almacenados en todos los nodos del árbol
        public int tamaño(){
            //Llama al método privado tamaño para adquirir el número de nodos
            return size(root);
        }

        private int size(NodoArbolB x){
            //Establece el int del tamaño y aumenta según cuantos nodos valores insertados contiene.
            int tamaño = 0;
            for(int j=0; j<x.n; j++){
                tamaño++;
            }
            //Comprueba si el nodo del árbol actual no es un nodo hoja
            if(!x.leaf){
                //Analiza cada hijo del nodo y sumará los valores que poseen cada uno
                for(int i=0; i<=x.n; i++){
                    if(x.child[i]!=null){
                        tamaño+= size(x.child[i]);
                    }
                }
            }
            //retornara el tamaño del arbol
            return tamaño;
        }

        public double utilizacion(){
            //Metodo que retorna el porcentaje de utilizacoion del arbol
            double valor = (double) tamaño()/spaces(root);
            //El resultado se retornará multiplicado por 100, mostrando el porcentaje al final.
            return valor*100;
        }

        private int spaces(NodoArbolB x){
            //Establece la variable int valor como el tamaño del arreglo de claves del nodo actual
            int valor = x.key.length;
            //Comprueba si el nodo del árbol actual no es un nodo hoja
            if(!x.leaf){
                for(int i=0; i<=x.n; i++){
                    //Analiza cada hijo del nodo y sumará los valores que poseen cada uno
                    if(x.child[i]!=null){
                        valor+= spaces(x.child[i]);
                    }
                }
            }
            //retorna el resultado de la suma de modos
            return valor;
        }
    }

    public static class NodoArbolB {

        int n; //numero de claves almacenadas en el nodo
        boolean leaf; //Si el nodo es hoja (nodo hoja=true; nodo interno=false)
        int key[];  //almacena las claves en el nodo
        NodoArbolB child[]; //arreglo con referencias a los hijos

        //Constructores
        public NodoArbolB(int t) {
            n = 0;
            leaf = true;
            key = new int[((2 * t) - 1)];
            child = new NodoArbolB[(2 * t)];
        }

        public void imprimir() {
            System.out.print("[");
            for (int i = 0; i < n; i++) {
                if (i < n - 1) {
                    System.out.print(key[i] + " | ");
                } else {
                    System.out.print(key[i]);
                }
            }
            System.out.print("]");
        }

        public int find(int k) {
            for (int i = 0; i < n; i++) {
                if (key[i] == k) {
                    return i;
                }
            }
            return -1;
        }
    }

    public static void main(String[] args) {
        //grado minimo del Arbol B es t=3
        //Cada nodo soporta 2t hijos y 2t-1 claves
        int t = 3;
        //Se crea el arbol B segun t
        ArbolB arbolB = new ArbolB(t);

        //Valores a ingresar primera ronda
        int[] valoresUno = {20, 10, 50, 30, 40};
        System.out.println("-- INICIO --");
        System.out.println("INSERTANDO VALORES AL ARBOL B");
        for(int i=0; i<valoresUno.length; i++) {
            System.out.println("Insertando... valor " + valoresUno[i]);
            arbolB.insertar(valoresUno[i]);
        }

        //Mostrando arbol B por pantalla en preorder
        System.out.println("ESTADO ARBOL B");
        arbolB.showBTree();
        System.out.println("");

        //Valores a ingresar segunda ronda
        System.out.println("Insertando... valor 60");
        arbolB.insertar(60);
        //Mostrando arbol B por pantalla en preorder
        System.out.println("ESTADO ARBOL B");
        arbolB.showBTree();
        System.out.println("");

        //Valores a ingresar tercera ronda
        System.out.println("Insertando... valor 80");
        arbolB.insertar(80);
        System.out.println("Insertando... valor 70");
        arbolB.insertar(70);
        System.out.println("Insertando... valor 90");
        arbolB.insertar(90);
        //Mostrando arbol B por pantalla en preorder
        System.out.println("ESTADO ARBOL B");
        arbolB.showBTree();
        System.out.println("");

        //Buscar
        System.out.println("\nBuscando el nodo con el valor 80 en el arbol B:");
        arbolB.buscarNodoPorClave(80);

        //IMPLEMENTAR
        System.out.println("\nEl valor maximo del arbol B es : " + arbolB.buscarClaveMayor());

        System.out.print("El nodo minimo de la raiz del arbol B es :");
        arbolB.mostrarClavesNodoMinimo();

        System.out.println("");
        System.out.println("-- FIN --");
    }
}
