package Lab12;

import java.util.ArrayList;
import java.util.List;

public class Lab12_Binomial_Heap {
    static class ColaBinomial {
        NodoCB head; // Puntero al primer árbol binomial de la cola

        ColaBinomial() {
            head = null;
        } // Constructor


        //==================================================
        //Inserción de un elemento en la cola binomial
        //==================================================
        void Insert(int a) {
            ColaBinomial H1 = new ColaBinomial();
            H1.head = new NodoCB(null, null, null, a, (short) 0);
            head = BinomialHeapUnion(H1);
        }

        //============ Imprime cola binomial ===================================
        //La salida de la impresión es código fuente Latex la cual
        //se puede rediccionar a un archivo. Por ejemplo, java lp > micola.tex.
        //Luego el archivo micola.tex se compila con latex micola.tex.
        //=======================================================================
        void Print() {
            System.out.println("\\documentclass[10pt]{article}");
            System.out.println("\\usepackage{graphicx}");
            System.out.println("\\usepackage{pstricks,pst-node,pst-tree}");
            System.out.println("\\title{Cola Binomial}");
            System.out.println("\\begin{document}");
            System.out.println("\\maketitle");
            for (NodoCB l = head; l != null; l = l.sibling) l.Print();
            System.out.println("\\end{document}");
        }

        //====================================================
        // Método privados
        //====================================================

        //====================================================
        //Unión (fusión de dos colas binomiales)
        //====================================================
        private NodoCB BinomialHeapUnion(ColaBinomial H2) {
            ColaBinomial H = BinomialHeapMerge(this, H2);
            if (H.head == null) return H.head;
            NodoCB prevx = null;
            NodoCB x = H.head;
            NodoCB nextx = x.sibling;
            while (nextx != null) {
                if ((x.degree != nextx.degree) || (nextx.sibling != null && nextx.sibling.degree == x.degree)) {
                    prevx = x;
                    x = nextx;
                } else if (x.key <= nextx.key) {
                    x.sibling = nextx.sibling;
                    nextx.BinomialLink(x);
                } else {
                    if (prevx == null) H.head = nextx;
                    else prevx.sibling = nextx;
                    x.BinomialLink(nextx);
                    x = nextx;
                }
                nextx = x.sibling;
            }
            return H.head;
        }

        //===========================================================
        // Mezcla de dos colas Binomiales. A partir de dos colas
        // se obtine una tercera que contiene los árboles binomiales
        // de las dos colas ordenados por k
        //===========================================================
        private ColaBinomial BinomialHeapMerge(ColaBinomial H1, ColaBinomial H2) {
            NodoCB h1 = H1.head;
            NodoCB h2 = H2.head;
            if (h1 == null) return H2;
            if (h2 == null) return H1;
            ColaBinomial H = new ColaBinomial();
            NodoCB ini = null;
            NodoCB fin = null;
            while (h1 != null && h2 != null) {
                if (h1.degree <= h2.degree) {
                    if (ini == null) {
                        ini = fin = h1;
                    } else {
                        fin.sibling = h1;
                        fin = h1;
                    }
                    h1 = h1.sibling;
                } else {
                    if (ini == null) {
                        ini = fin = h2;
                    } else {
                        fin.sibling = h2;
                        fin = h2;
                    }
                    h2 = h2.sibling;
                }
            }
            if (h1 == null) fin.sibling = h2;
            if (h2 == null) fin.sibling = h1;
            H.head = ini;
            return H;
        }

        public int size() {
            //Costo Algoritmico O(nLogn)
            //no es una funcion recursiva, posee un bucle while y se repite la cantidad de veces que requiera el programa
            // entrega que costo algoritmico es nLogn, ya que los datos son aleatorios y debe ordenarlos y luego insertarlos
            //adecuadamente segun la estrutura del Binomial Heap
            int cont = 0;
            List<NodoCB> nodos = new ArrayList<NodoCB>();
            if (head != null) {
                nodos.add(head);
                cont++;
            }
            //Verifica que no sea nulo el nodo
            while (!nodos.isEmpty()) {
                NodoCB nodo = nodos.get(0);
                nodos.remove(0);
                //Comprueba si no es nulo el primo
                if (nodo.sibling != null) {
                    //Lo añade a la lista
                    nodos.add(nodo.sibling);
                    cont++;
                }
                //Comprueba si no es nulo el hijo
                if (nodo.child != null) {
                    //Lo añade a la lista
                    nodos.add(nodo.child);
                    cont++;
                }
            }
            //retorna el tamaño
            return cont;
        }
    }

    static class NodoCB {
        NodoCB p; // El padre
        NodoCB sibling; // el hermano
        NodoCB child;  // el hijo
        Integer key;  // la clave
        Short degree; // el grado.

        //============================
        //Constructor
        //============================
        NodoCB(NodoCB p, NodoCB sibling,
               NodoCB child, Integer key,
               Short degree) {
            this.p = p;
            this.sibling = sibling;
            this.child = child;
            this.key = key;
            this.degree = degree;
        }

        //===============================
        //Une dos binomiales árboles B_k
        //===============================
        void BinomialLink(NodoCB z) {
            p = z;
            sibling = z.child;
            z.child = this;
            z.degree++;
        }

        //==================================
        //Imprime un árbol binomial
        //==================================
        void Print() {
            System.out.println("\\pstree[levelsep=25pt]{\\Tcircle{" + key + "}}"); // La salida es códgio latex
            System.out.println("{"); // Salida código latex
            for (NodoCB l = child; l != null; l = l.sibling)
                l.Print(); //impresión recursiva de los hijos del nodo
            System.out.println("}");// Salida código latex

        }
    }

    public static void main(String[] args) {
        int a[] = {12, 17, 2, 4, 5, 6, 22, 21, 29, 32, 40, 45, 3, 54, 65};
        ColaBinomial b = new ColaBinomial();
        for (int i = 0; i < a.length; i++) b.Insert(a[i]);
        b.Print();
        System.out.println("El tamaño del Binomial Heap es " + b.size());
    }
}
