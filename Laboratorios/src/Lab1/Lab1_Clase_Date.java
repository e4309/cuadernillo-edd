package Lab1;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.Scanner;

public class Lab1_Clase_Date {
    public static class Date {

        /*
         * data --> 32 bits
         * [0, 11] --> 12 bits --> año
         * [12, 15] --> 4 bits --> mes
         * [16, 20] --> 5 bits --> dia
         * [21, 25] --> 5 bits --> hora
         * [26, 31] --> 6 bits --> minuto
         */
        //Inicializa un entero equivalente a 32 bits llamado data
        private int data;

        //Inicializa el estatico del patron singleton
        private static Date instance = null;

        //Inicializa el acceso a la instacia de singleton
        public static Date getInstance() {
            if (instance == null) {
                instance = new Date();
            }
            return instance;
        }

        //Inicializa el entero data en 0
        public Date() {
            data = 0;
        }

        //Obtiene el año y lo inserta en el arreglo de bits de data
        public int getAño() {
            int año = data >>> 20;
            return año;
        }

        //Obtiene el mes y lo inserta en el arreglo de bits de data
        public int getMes() {
            int mes = (data << 12) >>> 28;
            return mes;
        }

        //Obtiene el dia y lo inserta en el arreglo de bits de data
        public int getDia() {
            int dia = (data << 16) >>> 27;
            return dia;
        }

        //Obtiene la hora y lo inserta en el arreglo de bits de data
        public int getHora() {
            int hora = (data << 21) >>> 27;
            return hora;
        }

        //Obtiene los minuto y los inserta en el arreglo de bits de data
        public int getMinuto() {
            int minuto = (data << 26) >>> 26;
            return minuto;
        }

        //Genera el año como cadena de bits con datos recibidos desde teclado
        public void setAño(int año) {
            if (año >= 0 && año < 4096) {
                int mask = 1048575; //00000000000011111111111111111111
                data = (data & mask) | (año << 20);
                System.out.println("DATA: " + Long.toBinaryString(data));
            } else {
                System.out.println("Año no ingresado");
            }
        }

        //Genera el mes como cadena de bits con datos recibidos desde teclado
        public void setMes(int mes) {
            if (mes > 0 && mes < 13) {
                int mask = 2146500607 | (1 << 31); //01111111111100001111111111111111
                data = (data & mask) | (mes << 16);
                System.out.println("DATA: " + Long.toBinaryString(data));
            } else {
                System.out.println("Mes no ingresado");
            }
        }

        //Genera el dia como cadena de bits con datos recibidos desde teclado
        public void setDia(int dia) {
            if (dia > 0 && dia < 32) {
                int mask = 2147420159 | (1 << 31); //01111111111111110000011111111111
                data = (data & mask) | (dia << 11);
                System.out.println("DATA: " + Long.toBinaryString(data));
            } else {
                System.out.println("Dia no ingresado");
            }
        }

        //Genera la hora como cadena de bits con datos recibidos desde teclado
        public void setHora(int hora) {
            if (hora >= 0 && hora < 24) {
                int mask = 2147481663 | (1 << 31); //01111111111111111111100000111111
                data = (data & mask) | (hora << 6);
                System.out.println("DATA: " + Long.toBinaryString(data));
            } else {
                System.out.println("Hora no ingresada");
            }
        }

        //Genera los minutos como cadena de bits con datos recibidos desde teclado
        public void setMinuto(int minuto) {
            if (minuto >= 0 && minuto < 60) {
                int mask = 2147483584 | (1 << 31); //01111111111111111111111111000000
                data = (data & mask) | (minuto);
                System.out.println("DATA: " + Long.toBinaryString(data));
            } else {
                System.out.println("Minuto no ingresado");
            }
        }

        //Verifica si la fecha ingresda es igual a la del computador
        public boolean sameDate(LocalDate t) {
            LocalDate x = LocalDate.now();
            if (x.equals(t)) {
                return true;
            } else {
                return false;
            }
        }

        //Verifica si la fecha ingresada es anterior a la del computador
        public boolean isBefore(LocalDate t) {
            LocalDate x = LocalDate.now();
            if (x.isBefore(t)) {
                return true;
            } else {
                return false;
            }
        }

        //Verifica si la fecha ingresada es posterior a la del computador
        public boolean isAfter(LocalDate t) {
            LocalDate x = LocalDate.now();
            if (x.isAfter(t)) {
                return true;
            } else {
                return false;
            }
        }

        //Funcion To String que devuelve un string los datos ingresados
        @Override
        public String toString() {
            return getDia() + "/" + getMes() + "/" + getAño() + " " + getHora() + ":" + getMinuto();
        }

        public static void main(String[] args) {
            Scanner tec = new Scanner(System.in);
            System.out.println("Ingrese el dia: ");
            int dia = tec.nextInt();
            Date.getInstance().setDia(dia);
            System.out.println("Ingrese el mes: ");
            int mes = tec.nextInt();
            Date.getInstance().setMes(mes);
            System.out.println("Ingrese el año: ");
            int año = tec.nextInt();
            Date.getInstance().setAño(año);
            System.out.println("Ingrese la hora: ");
            int hora = tec.nextInt();
            Date.getInstance().setHora(hora);
            System.out.println("Ingrese los minutos: ");
            int minuto = tec.nextInt();
            Date.getInstance().setMinuto(minuto);
            System.out.println(Date.getInstance().toString());
        }
    }
}
